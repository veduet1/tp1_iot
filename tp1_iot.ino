/*
  Lora Send And Receive
  This sketch demonstrates how to send and receive data with the MKR WAN 1300/1310 LoRa module.
  This example code is in the public domain.
*/
#include "src/mkrwan/utilities.h"
#include "src/mkrwan/MKRWAN.h"

#define MAGIC_NUMBER 0x3F

LoRaModem modem;

void retrieveCredentials() {
  while (1) {
    print("DEVEUI > ");
    
    const String deviceEUI = Serial.readStringUntil('\n');

    if (!checkHexaStr(deviceEUI, 8)) {
      print("Error : Invalid DEVEUI `", deviceEUI, "`");
    } else {
      modem.setDeviceEui(deviceEUI);
      break;
    }
  }

  while (1) {
    print("APPEUI > ");
    
    const String appEui = Serial.readStringUntil('\n');

    if (!checkHexaStr(appEui, 8)) {
      print("Error : Invalid APPEUI `", appEui, "`");
    } else {
      modem.setAppEui(appEui);
      break;
    }
  }

  while (1) {
    print("APPKEY > ");

    const String appKey = Serial.readStringUntil('\n');

    if (!checkHexaStr(appKey, 16)) {
      print("Error : Invalid APPKEY `", appKey, "`");
    } else {
      modem.setAppKey(appKey);
      break;
    }
  }
    
  print("APPKEY : ", modem.getAppKey());
}

void setup() {
  // Initializing serial
  Serial.begin(115200);
  Serial.setTimeout(10000);
  while (!Serial);

  // change this to your regional band (eg. US915, AS923, ...)
  if (!modem.begin(LoRaBand::EU868)) {
    debug("Error: Failed to start module");

    while (1) delay(1);
  };

  debug("Your module version is: ", modem.version());

  char hint;

  if (!modem.readFromNVM(hint)) {
    debug("Error: Unable to read hint from NVM");

    while (1) delay(1);
  }

  if (hint != MAGIC_NUMBER) {
    retrieveCredentials();

    hint = MAGIC_NUMBER;
  
    if (!modem.writeToNVM(hint)) {
      debug("Error: Unable to write NVM");
    }
  }

  int connected = modem.joinOTAA();
  if (!connected) {
    print("Something went wrong; are you indoor? Move near a window and retry");

    while (1) delay(1);
  }

  // Set poll interval to 60 secs.
  modem.minPollInterval(60);
  // NOTE: independent of this setting, the modem will
  // not allow sending more than one message every 2 minutes,
  // this is enforced by firmware and can not be changed.
}

void loop() {  
  int err;
  modem.beginPacket();
  modem.print((double)125.54);
  err = modem.endPacket(true);
  
  if (err > 0) {
    print("Message sent correctly!");
  } else {
    print("Error sending message :(");
    print("(you may send a limited amount of messages per minute, depending on the signal strength");
    print("it may vary from 1 message every couple of seconds to 1 message every minute)");
  }
  
  delay(60000000);
}
